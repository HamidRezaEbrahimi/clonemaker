import json
import os
from servicedata import GetServiceData
from config import Config
from condition import Condition
from clone import CloneMng
from sysCtl import SystemdService
from apigwconfig import RemoteClient
from osinfo import OsCheck
import schedule
import time


def main():
    with open(config.JsonFile) as datafile:
        data = json.load(datafile)
        for Dict in data:
            data = GetServiceData(Dict)
            condition = Condition(
                serviceopenfile=data.openFiles, warnopendile=config.WarnOpenFile, erroropenfile=config.ErrOpenFile
            )
            check = condition.CheckOpenFile()
            checkClone = CloneMng(serviceName=data.serviceName, MaxClone=config.MaxClone)
            matchTotalClone = config.CloneDir + checkClone.service + '-c*'
            cloneAvail, totalClone = checkClone.CheckClone(matchTotalClone)
            for x in cloneAvail:
                cloneNumber = str(cloneAvail[0])

            cloneName = checkClone.service + "-c" + cloneNumber
            link = config.CloneDir + cloneName
            serviced = config.SystemCtl + cloneName + ".services"
            src = config.ServiceDir + checkClone.service + "/" + "current"
            dst = link + "/" + "current"
            checkExitingLink = checkClone.CheckLinkExist(link)
            checkExitingServiced = checkClone.CheckServiceExist(serviced)
            serviceCnf = config.ServiceConf + checkClone.service + ".properties"
            sampleServiced = config.SampleService
            if check is not True:
                if totalClone <= config.MaxClone and osInfo.memFree > config.FreeRam:
                    if check == "Error":
                        if checkExitingLink and checkExitingServiced:
                            defaultPort = config.GetDefaultPort(serviceCnf)
                            clonePort = config.MakeClonePort(defaultPort, cloneNumber)
                            os.mkdir(link)
                            os.symlink(src, dst)
                            checkClone.CreateService(serviced, cloneName, clonePort, sampleServiced)
                            cloneSystemd = SystemdService(cloneName, config.RemotePassWord)
                            cloneSystemd.start()
                            apiGwConfig = RemoteClient(config.RemoteAddr, config.RemoteUser, config.RemotePassWord)
                            upstreamConf = config.UpstreamConf + data.serviceName + ".ups"
                            addConfig = f'echo {config.RemotePassWord} | sudo -S sed -i "\$i $(echo -e ' \
                                        f'"\ \ \ \ \ \ server {serverIp}:{clonePort}\;")" {upstreamConf}'
                            checkConfig = f'echo {config.RemotePassWord} | sudo -S nginx -t'
                            reloadConfig = f'echo {config.RemotePassWord} | sudo -S systemctl reload nginx'
                            apiGwConfig.execute_commands([addConfig, checkConfig, reloadConfig])
                            apiGwConfig.disconnect()
                        else:
                            print(f'restart {cloneName}')
                            cloneSystemd = SystemdService(cloneName, config.RemotePassWord)
                            cloneSystemd.restart()

                    elif check == "Warn":
                        if checkExitingLink and checkExitingServiced:
                            defaultPort = config.GetDefaultPort(serviceCnf)
                            clonePort = config.MakeClonePort(defaultPort, cloneNumber)
                            os.mkdir(link)
                            os.symlink(src, dst)
                            checkClone.CreateService(serviced, cloneName, clonePort, sampleServiced)
                            cloneSystemd = SystemdService(cloneName, config.RemotePassWord)
                            cloneSystemd.start()
                            apiGwConfig = RemoteClient(config.RemoteAddr, config.RemoteUser, config.RemotePassWord)
                            upstreamConf = config.UpstreamConf + data.serviceName + ".ups"
                            addConfig = f'echo {config.RemotePassWord} | sudo -S sed -i "\$i $(echo -e ' \
                                        f'"\ \ \ \ \ \ server {serverIp}:{clonePort}\;")" {upstreamConf}'
                            checkConfig = f'echo {config.RemotePassWord} | sudo -S nginx -t'
                            reloadConfig = f'echo {config.RemotePassWord} | sudo -S systemctl reload nginx'
                            apiGwConfig.execute_commands([addConfig, checkConfig, reloadConfig])
                            apiGwConfig.disconnect()

                        else:
                            print(f'restart {cloneName}')
                            cloneSystemd = SystemdService(cloneName, config.RemotePassWord)
                            cloneSystemd.restart()
                else:
                    print(f'restart {data.serviceName}')
                    cloneSystemd = SystemdService(data.serviceName, config.RemotePassWord)
                    cloneSystemd.restart()
            else:
                pass


if __name__ == '__main__':
    config = Config('/home/hamidreza/PycharmProjects/MakeCloneService/config/config.yaml')
    osInfo = OsCheck(config.MemFile)
    serverIp = osInfo.GetServerIp()
    schedule.every(config.ScheduleProgram).seconds.do(main)
    while True:
        schedule.run_pending()
        time.sleep(5)

