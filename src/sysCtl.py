import subprocess


class SystemdService:
    """ A systemd service object with methods to check it's activity, and to stop() and start() it. """

    def __init__(self, service, sudoPassword):
        self.service = service
        self.password = sudoPassword

    def is_active(self):
        """Return True if systemd service is running"""
        try:
            cmd = 'echo {} | sudo -S /bin/systemctl status {}.service'.format(self.password, self.service)
            completed = subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE)
        except subprocess.CalledProcessError as err:
            print('ERROR:', err)
        else:
            for line in completed.stdout.decode('utf-8').splitlines():
                if 'Active:' in line:
                    if '(running)' in line:
                        print('True')
                        return True
            return False

    def stop(self):
        """ Stop systemd service."""
        try:
            cmd = 'echo {} | sudo -S /bin/systemctl stop {}.service'.format(self.password, self.service)
            completed = subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE)
            print(completed)
        except subprocess.CalledProcessError as err:
            print('ERROR:', err)

    def start(self):
        """ Start systemd service."""
        try:
            cmd = 'echo {} | sudo -S /bin/systemctl start {}.service'.format(self.password, self.service)
            completed = subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE)
            print(completed)
        except subprocess.CalledProcessError as err:

            print('ERROR:', err)

    def restart(self):
        """ reStart systemd service."""
        try:
            cmd = 'echo {} | sudo -S /bin/systemctl restart {}.service'.format(self.password, self.service)
            completed = subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE)
            print(completed)
        except subprocess.CalledProcessError as err:

            print('ERROR:', err)
