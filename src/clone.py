import re
import os
import glob


class CloneMng:

    def __init__(self, serviceName, MaxClone):
        self.lst = list(range(1, MaxClone + 1))
        self.MaxClone = MaxClone
        matchName = re.match(r"(.*)-c(\d)|(.*)", serviceName)
        if matchName.group(2) is None:
            self.service = (matchName.group(3))
            self.cloneNo = 0
            self.fullName = self.service

        elif int(matchName.group(2)) >= 1:
            self.service = (matchName.group(1))
            self.cloneNo = int((matchName.group(2)))
            self.fullName = self.service + '-c' + str(self.cloneNo)

    def CheckClone(self, clonePath):
        lst = []
        cloneDir = clonePath
        cloneExist = glob.glob(cloneDir)
        totalClone = len(cloneExist)
        for path in cloneExist:
            matchName = re.match(r"(.*)-c(\d)", path)
            No = int(matchName.group(2))
            lst.append(No)
        cloneAvail = list(set(self.lst).difference(lst))
        return cloneAvail, totalClone

    @staticmethod
    def CheckLinkExist(cloneLink):
        if not os.path.isdir(cloneLink):
            return True
        else:
            return False

    @staticmethod
    def CheckServiceExist(servicedPath):
        if not os.path.isfile(servicedPath):
            return True
        else:
            return False

    @staticmethod
    def CreateService(servicedPath, cloneName, clonePort, sampleServiced):
        matchNumber = re.match(r"(.*)-c(\d)|(.*)", cloneName)
        portNo = 'SERVER_PORT=' + clonePort
        number = matchNumber.group(2)
        name = matchNumber.group(1)

        print(number)
        with open(sampleServiced, 'r') as file:
            print(servicedPath)
            line = file.read()

            line = line.replace('SERVER_PORT=0000', portNo)
            line = line.replace('serviceName', cloneName)
            line = line.replace(cloneName + ".jar", name + ".jar")
            line = line.replace('KARIZ_LOG_HOME=/path', 'KARIZ_LOG_HOME=/var/log/kariz/clones' + number)

            with open(servicedPath, 'w') as f:
                f.write(line)