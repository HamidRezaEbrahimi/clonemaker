class Condition:
    warnList = []

    def __init__(self, serviceopenfile, warnopendile, erroropenfile):
        self.serviceopenfile = serviceopenfile
        self.warnopendile = warnopendile
        self.erroropenfile = erroropenfile

    def CheckOpenFile(self):
        if self.serviceopenfile >= self.warnopendile:
            if self.serviceopenfile >= self.erroropenfile:
                return "Error"
            elif self.warnopendile <= self.serviceopenfile < self.erroropenfile:
                return "Warn"
        else:
            return True




