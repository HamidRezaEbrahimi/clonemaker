import re
import yaml


class Config:

    def __init__(self, yml):
        with open(yml) as cfg:
            cnf = yaml.load(cfg, Loader=yaml.FullLoader)
            self.JsonFile = cnf['DataDir']['JsonFile']
            self.MemFile = cnf['DataDir']['MemFile']
            self.ServiceDir = cnf['DataDir']['ServiceDir']
            self.CloneDir = cnf['DataDir']['CloneDir']
            self.SystemCtl = cnf['DataDir']['SystemCtl']
            self.ServiceConf = cnf['DataDir']['ServiceConf']
            self.SampleService = cnf['DataDir']['SampleService']
            self.WarnTime = cnf['Check']['Time']['WarnTime']
            self.ErrTime = cnf['Check']['Time']['ErrTime']
            self.ScheduleProgram = cnf['Check']['Time']['ScheduleProgram']
            self.WarnOpenFile = cnf['Check']['OpenFile']['WarnOpenFile']
            self.ErrOpenFile = cnf['Check']['OpenFile']['ErrOpenFile']
            self.MaxClone = cnf['Check']['Clone']['MaxClone']
            self.FreeRam = cnf['Check']['Ram']['FreeRam']
            self.CpuUsage = cnf['Check']['Cpu']['CpuUsage']
            self.RemoteAddr = cnf['ApiGw']['Address']['RemoteAddr']
            self.RemoteUser = cnf['ApiGw']['User']['RemoteUser']
            self.RemotePassWord = cnf['ApiGw']['PassWord']['RemotePassword']
            self.UpstreamConf = cnf['ApiGw']['UpstreamConf']['Path']

    @staticmethod
    def GetDefaultPort(serviceName):
        with open(serviceName, "r") as srvCnf:
            for line in srvCnf:
                portMatch = re.match(r"(server.port: )(.*)", line)
                if portMatch:
                    port = portMatch.group(2)
            return port

    @staticmethod
    def MakeClonePort(defaultPort, cloneNumber):
        clonePort = defaultPort + cloneNumber
        return clonePort

