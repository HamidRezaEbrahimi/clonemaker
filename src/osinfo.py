import os
import socket

from config import Config


class OsCheck:

    def __init__(self, memFile):
        meminfo = dict(
            (i.split()[0].rstrip(":"), int(i.split()[1]))
            for i in open(memFile).readlines()
        )
        statinfo = '''grep 'cpu ' /proc/stat  | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage }' '''
        self.memTotal = round(meminfo["MemTotal"] / (2 ** 10), 2)
        self.memFree = round(meminfo["MemFree"] / (2 ** 10), 2)
        self.memAvail = round(meminfo["MemAvailable"] / (2 ** 10), 2)
        self.memCached = round(meminfo["Cached"] / (2 ** 10), 2)
        self.memBuffers = round(meminfo["Buffers"] / (2 ** 10), 2)
        self.CPU_Pct = str(round(float(os.popen(statinfo).readline()), 2))

    def RamInfo(self):
        return self.memTotal, self.memFree, self.memAvail, self.memCached, self.memBuffers

    def RamPct(self):
        return (self.memTotal - (self.memBuffers + self.memCached + self.memFree)) / self.memTotal * 100

    def CpuPct(self):
        return self.CPU_Pct

    @staticmethod
    def GetServerIp():
        hostname = socket.gethostname()
        local_ip = socket.gethostbyname(hostname)
        return local_ip
