class GetServiceData:

    def __init__(self, Dict):

        self.id = Dict['id']
        self.serviceName = Dict['name']
        self.pid = Dict['pid']
        self.threads = Dict['threads']
        self.memUsage = Dict['memory']
        self.cpuUsage = Dict['cpu']
        self.maxOpenFiles = Dict['maxOpenFiles']
        self.openFiles = Dict['openFiles']
        self.activity = Dict['activity']